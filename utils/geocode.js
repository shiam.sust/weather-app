const request = require('request')

const geocode = (address, callback) => {
    const url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/'+ encodeURIComponent(address) +'.json?access_token=pk.eyJ1Ijoic2hpYW0tc3VzdCIsImEiOiJja2Z0d2ppYmMwYXgzMnRwa2xkdmg3NHZ5In0.e6peCg78GIwU8A0sjxPj6w'
    request({ url: url, json: true}, (error, response) => {
        if(error){
            callback('Unable to connect to location services!', undefined)
        }else if(response.body.features.length === 0){
            callback('Unable to find location.Try another search.', undefined)
        }else{
            callback('undefined', {
                latitude: response.body.features[0].geometry.coordinates[1],
                longitude: response.body.features[0].geometry.coordinates[0]
            })
        }
    })
}

module.exports = geocode