const request = require('request')

const forecast = (lat, lon, callback) => {
    const url = 'https://api.openweathermap.org/data/2.5/onecall?lat='+lat+'&lon='+lon+'&exclude={part}&appid=237cf4fdbd654e5c2082c1607e0aeba1'
    request({ url: url, json: true}, (error, response) => {
        if(error){
            callback('Unable to connect to location services!', undefined)
        }else if(response.body.current.weather[0].length === 0){
            callback('Unable to find weather info.', undefined)
        }else{
            const message = 'Temperature is: ' + response.body.current.temp + ' weather condition is : ' + response.body.current.weather[0].description
            callback('undefined', {
                message: message
            })
        }
    })
}

module.exports = forecast